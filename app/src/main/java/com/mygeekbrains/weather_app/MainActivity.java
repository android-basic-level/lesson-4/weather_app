package com.mygeekbrains.weather_app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.mygeekbrains.weather_app.dataprovider.WeatherModel;
import com.mygeekbrains.weather_app.dataprovider.WeatherDataProvider;

public class MainActivity extends AppCompatActivity {

    /* элементы управления */
    private Spinner sprCities;
    private Button btnShow;

    private WeatherDataProvider provider = new WeatherDataProvider();
    private WeatherModel currentModel = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindUiControls();
        restoreVariable(savedInstanceState);
        refreshUiControlsData();
    }

    private void bindUiControls() {
        ArrayAdapter<String> sprArrayAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, provider.getCities());

        sprCities = findViewById(R.id.sprCities);
        sprCities.setAdapter(sprArrayAdapter);
        sprCities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                refreshCurrentModel(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnShow = findViewById(R.id.btnShow);
        btnShow.setOnClickListener(v -> {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.DETAILS, this.currentModel);
            startActivity(intent);
        });
    }

    private void refreshCurrentModel(int ndx) {
        this.currentModel = provider.getByCity(provider.getCities()[ndx]);
        this.refreshUiControlsData();
    }

    @Override
    protected void onSaveInstanceState(Bundle stateToSave) {
        stateToSave.putString(WeatherModel.KEY_CITY, this.currentModel.city);
        stateToSave.putInt(WeatherModel.KEY_TEMPERATURE, this.currentModel.temperature);
        stateToSave.putInt(WeatherModel.KEY_WETNESS, this.currentModel.wetness);
        stateToSave.putInt(WeatherModel.KEY_OVERCAST, this.currentModel.overcast);
        stateToSave.putString(WeatherModel.KEY_IMAGE, this.currentModel.image);

        super.onSaveInstanceState(stateToSave);
    }

    private void restoreVariable(Bundle stateToRestore) {
        if (stateToRestore == null) {
            return;
        }

        this.currentModel = new WeatherModel(stateToRestore.getString(WeatherModel.KEY_CITY),
                stateToRestore.getInt(WeatherModel.KEY_TEMPERATURE),  stateToRestore.getInt(WeatherModel.KEY_WETNESS),
                stateToRestore.getInt(WeatherModel.KEY_OVERCAST), stateToRestore.getString(WeatherModel.KEY_IMAGE));

    }

    private void refreshUiControlsData() {
        if (this.currentModel == null) {
            this.currentModel = provider.getByCity(provider.getCities()[0]);
        }
    }

}
